//
//  MemoriesTableViewCell.swift
//  MentalHealth
//
//  Created by Francesco Tito on 13/11/2019.
//  Copyright © 2019 Power Rangers. All rights reserved.
//

import UIKit

class MemoriesTableViewCell: UITableViewCell {
    
    public var containerView: UIView = {
        let container = UIView()
        container.translatesAutoresizingMaskIntoConstraints = false
        return container
    }()
    
    private var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        return stackView
    }()
    
    private var button : UIButton = {
        let button = UIButton()
        button.setBackgroundImage(UIImage(named: "Symbol"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        //button.backgroundColor = .red
        return button
    }()
    
    private var title : UILabel = {
        let someText = UILabel()
        someText.translatesAutoresizingMaskIntoConstraints = false
        someText.textColor = .black
        someText.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        //someText.numberOfLines = 0
        return someText
    }()
    
    private var date : UILabel = {
        let date = UILabel()
        date.translatesAutoresizingMaskIntoConstraints = false
        date.textColor = #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1)
        date.font = UIFont(name: "HelveticaNeue", size: 15)
        return date
    }()
    
    private var label : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1)
        label.font = UIFont(name: "HelveticaNeue", size: 12)
        label.numberOfLines = 3
        return label
    }()

    private var imageMemory : UIImageView = {
        let imageMemory = UIImageView()
        imageMemory.layer.masksToBounds = true
        imageMemory.layer.cornerRadius = 15
        imageMemory.contentMode = .scaleAspectFill
        return imageMemory
    }()
    
    
    private var imageNewMemory : UIImageView = {
        let imageNewMemory = UIImageView()
        imageNewMemory.translatesAutoresizingMaskIntoConstraints = false
        imageNewMemory.contentMode = .scaleAspectFill
        return imageNewMemory
    }()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUpLayout()
        setUpContainerView()
    
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setUpLayout() {
        
        selectionStyle = .none
        
        title.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        imageMemory.translatesAutoresizingMaskIntoConstraints = false
        
        
        stackView.addArrangedSubview(title)
        stackView.addArrangedSubview(date)
        //stackView.addArrangedSubview(stackView2)
        //stackView.addArrangedSubview(label)
        
        
        containerView.addSubview(imageMemory)
        containerView.addSubview(stackView)
        containerView.addSubview(label)
        containerView.addSubview(button)
        containerView.addSubview(imageNewMemory)
        
        addSubview(containerView)
        
        NSLayoutConstraint.activate([
            
            containerView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0),
            containerView.heightAnchor.constraint(equalToConstant: 150),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            
            imageMemory.widthAnchor.constraint(equalToConstant: 150),
            imageMemory.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            //imageMemory.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0),
            imageMemory.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            imageMemory.heightAnchor.constraint(equalToConstant: 150),
            //imageMemory.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0),
            
            stackView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 17),
            stackView.leadingAnchor.constraint(equalTo: imageMemory.trailingAnchor, constant: 15),
            stackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            stackView.bottomAnchor.constraint(equalTo: label.topAnchor, constant: -10),
            
            
            label.leadingAnchor.constraint(equalTo: imageMemory.trailingAnchor, constant: 15),
            label.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -45),
            
            button.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            button.widthAnchor.constraint(equalToConstant: 10),
            button.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            
            imageNewMemory.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 1),
            imageNewMemory.heightAnchor.constraint(equalToConstant: 20),
            imageNewMemory.widthAnchor.constraint(equalToConstant: 20),
            imageNewMemory.leadingAnchor.constraint(equalTo: stackView.trailingAnchor, constant: -30)


        ])
    }
    
    func config(subtitle: String, title: String, date : Date, image: UIImage, rotateImage : Bool) {
        let tr = CGAffineTransform.identity.rotated(by: .pi/2)
        imageMemory.transform = tr
        imageMemory.image = image
        let formatter = DateFormatter()
        label.text = subtitle
        self.title.text = title
        formatter.dateFormat = "dd-MMM-yyyy HH:mm:ss" 
        self.date.text = formatter.string(from: date)
        formatter.dateFormat = "dd-MMM-yyyy"
        if formatter.string(from: date) == formatter.string(from: Date()){
            imageNewMemory.image = UIImage(named: "new")
        }
            
    }
    
    func setUpContainerView(){
        containerView.layer.cornerRadius = 15
        containerView.backgroundColor = .white
        containerView.addShadowView()
    }
    
}


