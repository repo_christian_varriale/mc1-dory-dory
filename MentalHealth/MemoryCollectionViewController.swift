//
//  MemoryCollectionViewController.swift
//  MentalHealth
//
//  Created by Francesco Tito on 14/11/2019.
//  Copyright © 2019 Power Rangers. All rights reserved.
//

import UIKit



class MemoryCollectionViewController: UICollectionViewController {
    
  
    var memoryUser : [Memory] = []
       
    override func viewDidLoad() {
        super.viewDidLoad()
        for _ in 1...10{
            memoryUser.append(Memory())
        }

    }

    
    // MARK: UICollectionViewDataSource

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return memoryUser.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "memoryCell", for: indexPath) as!  MemoryCollectionViewCell
    
        cell.titleLabel.text = memoryUser[indexPath.item].titleLabel
        cell.textLabel.text = memoryUser[indexPath.item].someText
        cell.memoryImage.image = UIImage(named: memoryUser[indexPath.item].imageContent)
        cell.backgroundColor = .blue
        return cell
    }

}
