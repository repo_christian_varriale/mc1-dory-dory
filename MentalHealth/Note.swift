//
//  Note.swift
//  MentalHealth
//
//  Created by Михаил on 13.11.2019.
//  Copyright © 2019 Power Rangers. All rights reserved.
//

import Foundation

class Note {
    var contentText: String
    var writingDate: Date
    
    init(text:String) {
        contentText = text
        writingDate = Date()
    }
}
