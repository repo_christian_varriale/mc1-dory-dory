//
//  Emotion&ActivityList.swift
//  MentalHealth
//
//  Created by Михаил on 17.11.2019.
//  Copyright © 2019 Power Rangers. All rights reserved.
//

import Foundation
import UIKit

struct Emotion {
    var emoji: UIImage
    var description: String
    
    init(emoji: UIImage, description: String){
        self.emoji = emoji
        self.description = description
    }
}

struct ActivityUser {
    var emoji: UIImage
    var description: String
    
     init(emoji: UIImage, description: String){
           self.emoji = emoji
           self.description = description
    }
}
