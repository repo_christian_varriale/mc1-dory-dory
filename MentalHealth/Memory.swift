//
//  Memory.swift
//  MentalHealth
//
//  Created by Francesco Tito on 13/11/2019.
//  Copyright © 2019 Power Rangers. All rights reserved.
//

import Foundation
import UIKit

class Memory{
    var titleLabel : String
    var someText : String
    var imageContent : UIImage
    var activity : String
    var emoticon : String
    var date : Date
    var id : Int

    init(id : Int,titleLabel : String, someText : String, activity : String, emoticon : String, date: Date, imageContent : UIImage){
        self.id = id
        self.titleLabel = titleLabel
        self.someText = someText
        self.activity = activity
        self.emoticon = emoticon
        self.date = date
        self.imageContent = imageContent
    }
    
  
}
