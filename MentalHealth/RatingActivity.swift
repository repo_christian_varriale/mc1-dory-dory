//
//  Rating.swift
//  MentalHealth
//
//  Created by Christian Varriale on 16/11/2019.
//  Copyright © 2019 Power Rangers. All rights reserved.
//

import UIKit

@IBDesignable class RatingControlActivity: UIStackView {
    
    private var ratingButtons = [UIButton]()
    private let array: [String] = ["Act1", "Act2", "Act3", "Act4", "Act5"]
     
    var rating = 0
    
    //MARK: Inspectable - You can also specify properties that can then be set in the Attributes inspector. Add the @IBInspectable attribute to the desired properties
    @IBInspectable var starSize: CGSize = CGSize(width: 44.0, height: 44.0) {
        didSet {
            setupButtons()
        }
    }
     
    @IBInspectable var starCount: Int = 5 {
        didSet {
            setupButtons()
        }
    }

    //MARK: Inizialization - Right now, your initializers are placeholders that simply call the superclass’s implementation
    
    //A structure that contains the location and dimensions of a rectangle.
    override init(frame: CGRect) {
        super.init(frame:frame)    //superclass’s initializer
        
        setupButtons()
        
    }
    
    //An abstract class that serves as the basis for objects that enable archiving and distribution of other objects. NSCoder declares the interface used by concrete subclasses to transfer objects and other values between memory and some other format
    required init(coder:NSCoder){
        super.init(coder:coder)     //superclass’s initializer
        
        setupButtons()
    }
    
    //MARK: Button Action (Only for command Line)
    
    @objc func ratingButtonTapped(button: UIButton) {
        print("Button Activity pressed 👍")
    }
    
    //MARK: Private Methods - Private methods can only be called by code within the declaring class
    
    private func setupButtons() {
        
        // Clear any existing buttons
        for button in ratingButtons {
            removeArrangedSubview(button)
            button.removeFromSuperview()
        }
        ratingButtons.removeAll()
        
        // Load Button Images
        let bundle = Bundle(for: type(of: self))
        
        for element in 0..<starCount {
            
            let filledStar = UIImage(named: array[element], in: bundle, compatibleWith: self.traitCollection)
            // Create the button
            let button = UIButton()
            
            // Set the button images
            button.setImage(filledStar, for: .normal)
            
            // Add constraints
            /*
            Each line performs the following steps:

            1)The button’s heightAnchor and widthAnchor properties give access to layout anchors. You use layout anchors to create constraints—in this case, constraints that define the view’s height and width, respectively.
            2)The anchor’s constraint(equalToConstant:) method returns a constraint that defines a constant height or width for the view.
            3)The constraint’s isActive property activates or deactivates the constraint. When you set this property to true, the system adds the constraint to the correct view, and activates it.
             */
            
            button.translatesAutoresizingMaskIntoConstraints = false    //The first line of code disables the button’s automatically generated constraints
            button.heightAnchor.constraint(equalToConstant: starSize.height).isActive = true
            button.widthAnchor.constraint(equalToConstant: starSize.width).isActive = true
            
            // Setup the button action
            button.addTarget(self, action: #selector(RatingControlActivity.ratingButtonTapped(button:)), for: .touchUpInside)
            
            // Add the button to the stack - The addArrangedSubview() method adds the button you created to the list of views managed by the RatingControl stack view
            addArrangedSubview(button)
            
            // Add the new button to the rating button array
            ratingButtons.append(button)
        }
    }
}

