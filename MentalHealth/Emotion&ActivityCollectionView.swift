//
//  CollectionView.swift
//  MentalHealth
//
//  Created by Михаил on 17.11.2019.
//  Copyright © 2019 Power Rangers. All rights reserved.
//

import Foundation
import UIKit

class CollectionView: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return emotions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EACell", for: indexPath) as! EACell
        
//        let emotion = emotions[indexPath.row]
        
        cell.displayContent(image: emotions[indexPath.row].emoji, title: emotions[indexPath.row].description)
        
        cell.backgroundColor = .gray
        
        return cell
    }
    
    @IBOutlet weak var collectView: UICollectionView!
    
    private let reuseIdentifier = "EACell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeEmotions()
//        EACell.displayContent(emotions[].emoji, emotions[].description)
    }
    
    var emotions: [Emotion] = []
    var activities: [ActivityUser] = []
    
    var emotion_emoji_list = [#imageLiteral(resourceName: "Emoji1"), #imageLiteral(resourceName: "Emoji2"), #imageLiteral(resourceName: "Emoji3"), #imageLiteral(resourceName: "Emoji4"), #imageLiteral(resourceName: "Emoji5")]
    var emotion_desc_list = ["Shame", "Sadness", "No emotion", "Happiness", "Good"]

    func makeEmotions() {
        for i in 0...(emotion_emoji_list.count - 1) {
            emotions.append(Emotion(emoji: emotion_emoji_list[i], description: emotion_desc_list[i]))
        }
    }
    
    var emotionIndexPath: IndexPath? {
        didSet {
            var indexPaths: [IndexPath] = []
            if let emotionIndexPath = emotionIndexPath {
                indexPaths.append(emotionIndexPath)
            }
            
            if let oldValue = oldValue {
                indexPaths.append(oldValue)
            }
            collectView.performBatchUpdates({self.collectView.reloadItems(at: indexPaths)
                
            }) { _ in
                if let emotionIndexPath = self.emotionIndexPath {
                    self.collectView.scrollToItem(at: emotionIndexPath, at: .centeredVertically, animated: true)
                }
            }
        }
    }
}

extension CollectionView {
    func collectionView(_ collectionView: UICollectionView,
                               shouldSelectItemAt indexPath: IndexPath) -> Bool {
    if emotionIndexPath == indexPath {
      emotionIndexPath = nil
    } else {
      emotionIndexPath = indexPath
    }

    return false
  }
}

