//
//  APickingViewController.swift
//  MentalHealth
//
//  Created by Михаил on 22.11.2019.
//  Copyright © 2019 Power Rangers. All rights reserved.
//

import Foundation
import UIKit

class APickingViewController: UICollectionViewController  {
let active = Active()
    
override func viewDidLoad() {
        
        super.viewDidLoad()
        
        active.makeActive()
        print(self.collectionView.isUserInteractionEnabled)
    }
}

extension APickingViewController {
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                collectionView.deselectItem(at: indexPath, animated: true)
                if let index = active.selectedActiveIndex,
                    let cell = collectionView.cellForItem(at: IndexPath(row: index, section: 0)) {
                    cell.backgroundColor = #colorLiteral(red: 0.6935172677, green: 0.4671925306, blue: 0.592478931, alpha: 0.8470588235)
                    print("cell non selected")
        
                }
                active.selectActive(at: indexPath)
        
                let cell = collectionView.cellForItem(at: indexPath)
                print(active.selectedActive)
                cell?.backgroundColor = #colorLiteral(red: 0.6935172677, green: 0.4671925306, blue: 0.592478931, alpha: 0.8470588235)
                print("\(indexPath.row): cell selected")
        
            performSegue(withIdentifier: "aunwind", sender: cell)
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return active.numberOfActives()
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActiveCell", for: indexPath) as! ActiveCell
        
        cell.displayContent(image: active.activities[indexPath.row].image, title: active.activities[indexPath.row].descript)
        
        if indexPath.item == active.selectedActiveIndex {
            cell.isSelected = true
        } else {
            cell.isSelected = false
        }
        
        return cell
    }
}
