//
//  AddMemoryViewController.swift
//  MentalHealth
//
//  Created by Christian Varriale on 16/11/2019.
//  Copyright © 2019 Power Rangers. All rights reserved.
//

import Foundation
import UIKit

//We’re going to implement the UIImagePickerController delegate and also the UINavigationController delegate. Remember delegates have handy pre-defined functions that we can use

class AddMemoryViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource{
    
    //MARK: Variable
    var imagePickerController : UIImagePickerController!
    var newMemoryInsert:Memory?
    var lineView = UIView(frame: CGRect(x: 0, y: 105, width: 383, height: 1.0))
    var lineView2 = UIView(frame: CGRect(x: 0, y: 310, width: 383, height: 1.0))
    var lineView3 = UIView(frame: CGRect(x: 0, y: 530, width: 383, height: 1.0))
    let formatter = DateFormatter()
    
//    MARK: For collectionViews
    
//    @IBOutlet weak var emotionLabel: UILabel!
//    @IBOutlet weak var activeLabel: UILabel!
/*
    var emot = "" {
      didSet {
        emotionLabel.text = emot
      }
    }
    
    var acti = "" {
         didSet {
           activeLabel.text = acti
         }
       }
 */
    
    var emot: String = ""
    var emotImage : UIImage = #imageLiteral(resourceName: "veryhappy")
    {
        didSet {
            emotionChoosenImage.image = emotImage
            emotionChoosenImage.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            emotionChoosenImage.layer.borderWidth = 1
            emotionChoosenImage.layer.cornerRadius = 10
        }
    }
    
    var actiImage : UIImage = #imageLiteral(resourceName: "shopping")
    {
        didSet {
            activeChoosenImage.image = actiImage
            activeChoosenImage.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            emotionChoosenImage.layer.borderWidth = 1
            emotionChoosenImage.layer.cornerRadius = 10
        }
    }
    
    var acti: String = ""
    
    @IBOutlet weak var activeChoosenImage: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var emotionChoosenImage: UIImageView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var galleryButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var plusButtonEmotion: UIButton!
    @IBOutlet weak var plusButtonActivity: UIButton!
    @IBOutlet weak var viewShadow: UIView!
    
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var labelMemory: UILabel!
    
    @IBOutlet weak var textView: UITextView!
    
//    MARK: CHRistian
    
    let emotion = Emotion()
    let active = Active()
    
    var emo:String = ""
    var act:String = ""
    
    override func viewDidLoad() {
/*        emotionChoosenImage.layer.borderWidth = 1
        emotionChoosenImage.layer.cornerRadius = 10
        emotionChoosenImage.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        
        
        activeChoosenImage.layer.borderWidth = 1
        activeChoosenImage.layer.cornerRadius = 10
        activeChoosenImage.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
*/
        
        emotionChoosenImage.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        emotionChoosenImage.layer.cornerRadius = 10
        
        activeChoosenImage.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        activeChoosenImage.layer.cornerRadius = 10
        
        textView.layer.cornerRadius = 5
        formatter.dateFormat = "dd-MMM-yyyy"
        labelDate.text = formatter.string(from: Date.init())
        
        buttonRadius(cameraButton)
        buttonRadius(plusButtonEmotion)
        buttonRadius(galleryButton)
        buttonRadius(saveButton)
        buttonRadius(plusButtonEmotion)
        buttonRadius(plusButtonActivity)
        
        viewShadow.layer.cornerRadius = 10;
        viewShadow.layer.masksToBounds = true;

        lineView.layer.borderWidth = 1.0
        lineView.layer.borderColor = #colorLiteral(red: 0.8015473485, green: 0.8016827703, blue: 0.8015295267, alpha: 1)

        lineView2.layer.borderWidth = 1.0
        lineView2.layer.borderColor = #colorLiteral(red: 0.8015473485, green: 0.8016827703, blue: 0.8015295267, alpha: 1)

        lineView3.layer.borderWidth = 1.0
        lineView3.layer.borderColor = #colorLiteral(red: 0.8015473485, green: 0.8016827703, blue: 0.8015295267, alpha: 1)

        imageView.layer.cornerRadius = 20
//        imageView.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
//        imageView.layer.borderWidth = 1
        imageView.layer.masksToBounds = true
        imageView.addShadowView()
        imageView.contentMode = .scaleAspectFill
        
        saveButton.isHidden = true
        
        viewShadow.addShadowView2()
        viewShadow.addSubview(lineView)
        viewShadow.addSubview(lineView2)
        viewShadow.addSubview(lineView3)
        
        emotionChoosenImage.addShadowView()
        activeChoosenImage.addShadowView()
        
        emotion.makeEmotions()
        
        self.setupToHideKeyboardOnTapOnView()
        super.viewDidLoad()
        
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Layout Botton
    func buttonRadius(_ sender: UIButton){
        sender.layer.cornerRadius = 10
    }
    
    //function to come back in the view
    @IBAction func backInMemories(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func unwindWithSelectedGame(segue: UIStoryboardSegue) {
    
        if let pickingViewController = segue.source as? PickingViewController, let selectedEmotion = pickingViewController.emotion.selectedEmotion, let selectedEmotionImage = pickingViewController.emotion.selectedEmotionImage{
        
          emot = selectedEmotion
            emotImage = selectedEmotionImage
          print(emot, "ff")
      
        }
    }
    
    @IBAction func aUnwindWithSelectedGame(segue: UIStoryboardSegue) {
    
        if let apickingViewController = segue.source as? APickingViewController, let selectedActive = apickingViewController.active.selectedActive, let selectedActiveImage = apickingViewController.active.selectedActiveImage{
        
          acti = selectedActive
            actiImage = selectedActiveImage
          print(acti, "ff")
      
        }
    }
    
    @IBAction func onPhotoButton(_ sender: Any) {
        
        imageView.layer.borderWidth = 0
        imagePickerController = UIImagePickerController()   //This line creates an instance of UIImagePickerController
        imagePickerController.delegate = self               //This line assigns the delegate
        imagePickerController.sourceType = .camera          //This line specifies to use the camera
        present(imagePickerController, animated: true, completion: nil) //and this line actually starts up the UIImagePickerController
        doneButton.setTitleColor(#colorLiteral(red: 0.6935172677, green: 0.4671925306, blue: 0.592478931, alpha: 0.8470588235), for: .normal)
        
        if (self.imageView.image == nil){
            saveButton.isHidden = false
        }
    }
    
    @IBAction func onSaveButton(_ sender: Any) {
        
        if(self.imageView.image != nil){

        let imageData = imageView.image!.pngData()
        let compresedImage = UIImage(data: imageData!)
    
        UIImageWriteToSavedPhotosAlbum(compresedImage!, nil, nil, nil)
    
        let alert = UIAlertController(title: "Saved", message: "Your image has been saved", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func onGalleryButton(_ sender: Any) {
        
        imageView.layer.borderWidth = 0
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = .photoLibrary
        present(myPickerController, animated: true, completion: nil)
        doneButton.setTitleColor(#colorLiteral(red: 0.6935172677, green: 0.4671925306, blue: 0.592478931, alpha: 0.8470588235), for: .normal)
        saveButton.isHidden = false
        

    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
       imagePickerController?.dismiss(animated: true, completion: nil)
       imageView.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage

        //The first line “dismisses” the UIImagePickerViewController i.e. removes the camera screen and the second line stores in our imageView variable the image that was taken by the camera
        
    }
    
    @IBAction func ActButton(_ sender: UIButton) {
        switch sender.tag {
                    
                case 0:
                    act = "basket"
                case 1:
                    act = "book"
                case 2:
                    act = "drink"
                case 3:
                    act = "bike"
                case 4:
                    act = "sleep"
                case 5:
                    act = "shopping"
                default:
                    print("Otherwise, do something else.")
                }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showNewMemory", let title = textView.text, let image = imageView.image{
            
            newMemoryInsert = Memory(id: 0, titleLabel: "Memo Day ", someText: title, activity: act, emoticon: emo, date: Date(), imageContent: image)
            
        } else if segue.identifier == "PickEmotion", let title = textView.text, let image = imageView.image, let pickingViewController = segue.destination as? PickingViewController {
            
          pickingViewController.emotion.selectedEmotion = emot
          
          newMemoryInsert = Memory(id: 0, titleLabel: "Memo Day ", someText: title, activity: act, emoticon: emot, date: Date(), imageContent: image)
          print(emot)
            
        }else if segue.identifier == "PickActive", let title = textView.text, let image = imageView.image, let apickingViewController = segue.destination as? APickingViewController {
            
          apickingViewController.active.selectedActive = acti
          
          newMemoryInsert = Memory(id: 0, titleLabel: "Memo Day ", someText: title, activity: acti, emoticon: emo, date: Date(), imageContent: image)
//          print(acti)
            
        }
        
    }
    
    @IBAction func doneButton(_ unwindSegue: UIStoryboardSegue) {

        let title = textView.text
        let image = imageView.image
        
        //Control to understand if i have to take the correct emoticon in the view
        if (title != nil && image != nil && emo != "" && act != ""){
         newMemoryInsert = Memory(id: 0, titleLabel: "Memo Day ", someText: title!, activity: act, emoticon: emo, date: Date(), imageContent: image!)
        }else if (title != nil && image != nil && emo == "" && act == ""){
            newMemoryInsert = Memory(id: 0, titleLabel: "Memo Day ", someText: title!, activity: acti, emoticon: emot, date: Date(), imageContent: image!)
        }else if (title != nil && image != nil && emo == "" && act != ""){
            newMemoryInsert = Memory(id: 0, titleLabel: "Memo Day ", someText: title!, activity: act, emoticon: emot, date: Date(), imageContent: image!)
        }else if (title != nil && image != nil && emo != "" && act == ""){
            newMemoryInsert = Memory(id: 0, titleLabel: "Memo Day ", someText: title!, activity: acti, emoticon: emo, date: Date(), imageContent: image!)
        }
 
    }
    
    @IBAction func cancelButton(_ sender: UIButton) {
    }
    
    //MARK: Emotion for AddNewMemory
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
            collectionView.deselectItem(at: indexPath, animated: true)
        
            if let index = emotion.selectedEmotionIndex,
                let cell = collectionView.cellForItem(at: IndexPath(row: index, section: 0)) {
                    cell.backgroundColor = #colorLiteral(red: 0.976796329, green: 0.9521726966, blue: 1, alpha: 1)

                    print("cell non selected")
        
                }
                emotion.selectEmotion(at: indexPath)
        
                let cell = collectionView.cellForItem(at: indexPath)

                print(emotion.selectedEmotion)
                print("\(indexPath.row+1): cell selected")
        
        switch indexPath.row {
                    
                case 0:
                    emo = "sad"
                    emotImage = #imageLiteral(resourceName: "disappointed")
                case 1:
                    emo = "happy"
                    emotImage = #imageLiteral(resourceName: "happy")
                case 2:
                    emo = "angry"
                    emotImage = #imageLiteral(resourceName: "angry")
                default:
                    print("Otherwise, do something else.")
                }
        
//                emo = "Emoji\((indexPath.row+1))"
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return emotion.numberOfEmotions()
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmotionCell2", for: indexPath) as! EmotionCell2
        
        cell.displayContent(image: emotion.emotions[indexPath.row].image, title: emotion.emotions[indexPath.row].descript)
        cell.layer.cornerRadius = 10
        
/*        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 10
        cell.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
*/
        cell.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        if indexPath.item == emotion.selectedEmotionIndex {
            cell.isSelected = true
            cell.backgroundColor = #colorLiteral(red: 0.976796329, green: 0.9521726966, blue: 1, alpha: 1)
        } else {
            cell.isSelected = false
            cell.backgroundColor = #colorLiteral(red: 0.976796329, green: 0.9521726966, blue: 1, alpha: 1)
        }
        
        return cell
    }
    
}

extension UIViewController
{
    func setupToHideKeyboardOnTapOnView()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))

        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
}

