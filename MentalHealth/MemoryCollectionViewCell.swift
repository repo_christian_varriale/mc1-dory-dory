//
//  MemoryCollectionViewCell.swift
//  MentalHealth
//
//  Created by Francesco Tito on 14/11/2019.
//  Copyright © 2019 Power Rangers. All rights reserved.
//

import UIKit

class MemoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var memoryImage: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
}
