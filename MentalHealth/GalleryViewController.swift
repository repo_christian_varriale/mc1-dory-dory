//
//  GalleryViewController.swift
//  navigationworkshop
//
//  Created by Christian Varriale on 15/11/2019.
//  Copyright © 2019 prova. All rights reserved.
//

import UIKit

class GalleryViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getImage(imageName: "test.png")
        // Do any additional setup after loading the view.
    }
    
    func getImage(imageName: String){
        
       let fileManager = FileManager.default    //This creates an instance of the FileManager
       
       let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)    //This gets the path
        
       //This then checks the path for the image, if it exists it sets the imageView on the Gallery View Controller to that image
       if fileManager.fileExists(atPath: imagePath){
        
          imageView.image = UIImage(contentsOfFile: imagePath)
          imageView.transform = imageView.transform.rotated(by: CGFloat(Double.pi / 2))
       }else{
          print("Panic! No Image!")
       }
        
    }

}
