//
//  CoreDataController.swift
//  MentalHealth
//
//  Created by Francesco Tito on 17/11/2019.
//  Copyright © 2019 Power Rangers. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataController{
    let context: NSManagedObjectContext
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var listMemory: [Memory] = []
    var listId : Int = 0
    
    init(){
        context = appDelegate.persistentContainer.viewContext
    }
    
    func addMemory(date : Date, title: String, someText : String, activity : String, emoticon : String, image : UIImage){

        listId = lastId()+1
        guard let entity = NSEntityDescription.entity(forEntityName: "MemoryUser", in: context) else { fatalError("Error with entity")}
        
        let newMemory = NSManagedObject(entity: entity, insertInto: context)
        
        newMemory.setValue(listId , forKey: "id")
        newMemory.setValue(title, forKey: "title")
        newMemory.setValue(someText, forKey: "text")
        newMemory.setValue(activity, forKey: "activity")
        newMemory.setValue(emoticon, forKey: "emoction")
        newMemory.setValue(date, forKey: "date")
        newMemory.setValue(image.pngData(), forKey: "image")
        
        saveAdd()
    }
    
    func lastId() -> Int{
        return listMemory.count
    }
    
    func listMemoryUser() -> [Memory]{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName : "MemoryUser")
        request.returnsObjectsAsFaults = false
        do {
            
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                print(Int(data.value(forKey: "id") as! Int32))
                let memory = Memory(id: Int(data.value(forKey: "id") as! Int32), titleLabel: data.value(forKey: "title") as! String, someText: data.value(forKey: "text") as! String, activity: data.value(forKey: "activity") as! String, emoticon: data.value(forKey: "emoction") as! String, date: data.value(forKey: "date") as! Date, imageContent: UIImage(data: data.value(forKey: "image") as! Data)!)
                listMemory.append(memory)
                
          }
            
        } catch let error as NSError{
            
            print("Failed\(error)")
        }
        return listMemory
    }
    
    
    func saveAdd(){
        do {
           try context.save()
          } catch {
           print("Failed saving")
        }
    }
    
    
    func deleteRecords(){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MemoryUser")

        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

        do {
            try context.execute(batchDeleteRequest)

        } catch {
            print("Failed delete")
        }
    }
    
    func deleteRecord(memoryToDelete: Memory) {
        
    }
    
    
    
}
