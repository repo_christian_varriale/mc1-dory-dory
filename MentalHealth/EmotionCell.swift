//
//  EmotionCell.swift
//  MentalHealth
//
//  Created by Михаил on 22.11.2019.
//  Copyright © 2019 Power Rangers. All rights reserved.
//

import Foundation
import UIKit

class EmotionCell: UICollectionViewCell {

    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var labelCell: UILabel!
    
    override var isSelected: Bool {
            didSet {
//                backgroundColor = isSelected ? #colorLiteral(red: 0.976796329, green: 0.9521726966, blue: 1, alpha: 1) : .white
                picture.layer.borderWidth = isSelected ? 10 : 0
            }
        }
    
func displayContent(image: UIImage, title: String) {
        picture.image = image
        labelCell.text = title

    }
    
}

// FOR ADD NEW MEMORY
class EmotionCell2: UICollectionViewCell {
    
    @IBOutlet weak var picture2: UIImageView!
//    @IBOutlet weak var labelCell2: UILabel!
    
    override var isSelected: Bool {
            didSet {
                picture2.layer.borderWidth = isSelected ? 10 : 0
            }
        }
    
    func displayContent(image: UIImage, title: String) {
        picture2.image = image
//        labelCell2.text = title
    }
    
    
    
}

class ActiveCell: UICollectionViewCell {

    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var labelCell: UILabel!
    
    override var isSelected: Bool {
            didSet {
//                backgroundColor = isSelected ? #colorLiteral(red: 0.976796329, green: 0.9521726966, blue: 1, alpha: 1) : .white
                picture.layer.borderWidth = isSelected ? 10 : 0
            }
        }
    
func displayContent(image: UIImage, title: String) {
        picture.image = image
        labelCell.text = title

    }
    
}

// FOR ADD NEW MEMORY
class ActiveCell2: UICollectionViewCell {
    
    @IBOutlet weak var picture2: UIImageView!
//    @IBOutlet weak var labelCell2: UILabel!
    
    override var isSelected: Bool {
            didSet {
                picture2.layer.borderWidth = isSelected ? 10 : 0

            }
        }
    
    func displayContent(image: UIImage, title: String) {
        picture2.image = image

//        labelCell2.text = title
    }
    
    
    
}
