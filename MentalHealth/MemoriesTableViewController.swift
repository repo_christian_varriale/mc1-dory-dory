//
//  MemoriesTableViewController.swift
//  MentalHealth
//
//  Created by Francesco Tito on 13/11/2019.
//  Copyright © 2019 Power Rangers. All rights reserved.
//

import UIKit
import CoreData

class MemoriesTableViewController: UITableViewController , UISearchBarDelegate{
    
    @IBOutlet weak var searchBar: UISearchBar!
    let cellSpacingHeight: CGFloat = 180
    var rotateImage : Bool = true
    let core = CoreDataController()
    
    private var list :[Memory] = [] // used to update the table view when you search
    private var currentListArray : [Memory] = [] //used to update the table when you search
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.setupToHideKeyboardOnTapOnView()
        //core.deleteRecords()
        let notInvertedList = core.listMemoryUser()
        if notInvertedList.count > 0 {
            for i in 1...notInvertedList.count {
                       currentListArray.append(notInvertedList[notInvertedList.count-i])
                   }
        }
       
        
        //
        list = currentListArray
        //
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 25)!]
        navigationController?.navigationBar.barTintColor = UIColor(red:0.98, green:0.93, blue:0.96, alpha:1.0)
        searchBar.backgroundImage = UIImage()
        searchBar.backgroundColor = UIColor(red:0.98, green:0.93, blue:0.96, alpha:1.0)
        //navigationController?.navigationBar.prefersLargeTitles = true
        self.tableView.backgroundColor = UIColor(red:0.98, green:0.93, blue:0.96, alpha:1.0)
        self.becomeFirstResponder() // To get shake gesture
        setUpSearchBar()
        alterLayout()
    }
    
// mi fa il reload della table
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        tableView.reloadData()
//    }

    
    //
    func setUpSearchBar(){
        searchBar.delegate = self
        searchBar.doneAccessory = true
    }
    
    func alterLayout(){
        tableView.tableHeaderView = UIView()
        tableView.estimatedSectionHeaderHeight = 50
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else { list = currentListArray
            tableView.reloadData()
            return }
        list = currentListArray.filter({ memory -> Bool in
            return memory.someText.lowercased().contains(searchText.lowercased())
        })
        
        tableView.reloadData()
    }
    
//    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
//        <#code#>
//    }
    
    //
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! MemoriesTableViewCell
        cell.config(subtitle: list[indexPath.row].someText, title: list[indexPath.row].titleLabel, date: list[indexPath.row].date, image: list[indexPath.row].imageContent,rotateImage: rotateImage)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let showMemoriesViewController = storyBoard.instantiateViewController(identifier: "ShowMemories") as ShowMemoriesViewController
    
        showMemoriesViewController.memorySelected = list[indexPath.row]
        self.navigationController?.pushViewController(showMemoriesViewController, animated: true)
        
        
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return searchBar
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
//    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        return true
//    }
//    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//
//        if editingStyle == .delete {
//            print("Deleted")
//
//            self.list.remove(at: indexPath.row)
//            self.tableView.deleteRows(at: [indexPath], with: .fade)
//      }
//    }
    
    @IBAction func showNewMemory(_ segue: UIStoryboardSegue) {
        guard let addMemoryViewController = segue.source as? AddMemoryViewController,
        
        let newMemory = addMemoryViewController.newMemoryInsert else { return }
        rotateImage=false

        core.addMemory(date: newMemory.date, title: newMemory.titleLabel, someText: newMemory.someText, activity: newMemory.activity, emoticon: newMemory.emoticon, image: newMemory.imageContent)
        
        //list.insert(core.listMemoryUser().last!, at: 0)
        //tableView.insertRows(at: [IndexPath(row: list.count-1, section: 0)], with: .top)
        
        
        let indexPathOfFirstRow = NSIndexPath.init(row: 0, section: 0)
        list.insert(core.listMemoryUser().last!, at: 0)
        currentListArray.insert(core.listMemoryUser().last!, at: 0)
        //let cell = tableView.cellForRow(at: [0,0]) as! MemoriesTableViewCell
        
        tableView.beginUpdates()
        tableView.insertRows(at: [(indexPathOfFirstRow as IndexPath)], with: .automatic)
        tableView.endUpdates()
        //cell.containerView.backgroundColor = .lightGray
    }
    
    // We are willing to become first responder to get shake motion
    override var canBecomeFirstResponder: Bool {
        get {
            return true
        }
    }

    // Enable detection of shake motion
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {

        let next = self.storyboard?.instantiateViewController(withIdentifier: "AddMemoryViewController") as! AddMemoryViewController
        if motion == .motionShake {
            self.present(next, animated: true, completion: nil)
            print("Why are you shaking me?")
        }
    }
    
}

extension UIView {
    
    func addShadowView(isShadowPathEnabled: Bool = true, shadowColor: UIColor = .black, shadowRadius: CGFloat = 7, shadowOpacity: Float = 0.2, shadowOffset: CGSize = CGSize(width: 0.5, height: 0.5)) -> ShadowView {
        let shadowView = ShadowView()
        shadowView.translatesAutoresizingMaskIntoConstraints = false
        shadowView.isShadowPathEnabled = isShadowPathEnabled
        shadowView.shadowColor = shadowColor
        shadowView.shadowRadius = shadowRadius
        shadowView.shadowOpacity = shadowOpacity
        shadowView.shadowOffset = shadowOffset
        shadowView.layer.cornerRadius = layer.cornerRadius
        superview?.insertSubview(shadowView, belowSubview: self)
        
        shadowView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1).isActive = true
        shadowView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1).isActive = true
        shadowView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 10).isActive = true
        shadowView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 6).isActive = true
        
        return shadowView
    }
    
    func addShadowViewRotate(isShadowPathEnabled: Bool = true, shadowColor: UIColor = .black, shadowRadius: CGFloat = 7, shadowOpacity: Float = 0.2, shadowOffset: CGSize = CGSize(width: 0.5, height: 0.5)) -> ShadowView {
        let shadowView = ShadowView()
        shadowView.translatesAutoresizingMaskIntoConstraints = false
        shadowView.isShadowPathEnabled = isShadowPathEnabled
        shadowView.shadowColor = shadowColor
        shadowView.shadowRadius = shadowRadius
        shadowView.shadowOpacity = shadowOpacity
        shadowView.shadowOffset = shadowOffset
        shadowView.layer.cornerRadius = layer.cornerRadius
        superview?.insertSubview(shadowView, belowSubview: self)
        
        shadowView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1).isActive = true
        shadowView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1).isActive = true
        shadowView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 10).isActive = true
        shadowView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 6).isActive = true
        shadowView.transform = CGAffineTransform.identity.rotated(by: .pi/2)
        return shadowView
    }
    
    func addShadowView2(isShadowPathEnabled: Bool = true, shadowColor: UIColor = .black, shadowRadius: CGFloat = 7, shadowOpacity: Float = 0.5, shadowOffset: CGSize = CGSize(width: 0.5, height: 0.5)) -> ShadowView {
        let shadowView = ShadowView()
        shadowView.translatesAutoresizingMaskIntoConstraints = false
        shadowView.isShadowPathEnabled = isShadowPathEnabled
        shadowView.shadowColor = shadowColor
        shadowView.shadowRadius = shadowRadius
        shadowView.shadowOpacity = shadowOpacity
        shadowView.shadowOffset = shadowOffset
        shadowView.layer.cornerRadius = layer.cornerRadius
        superview?.insertSubview(shadowView, belowSubview: self)
        
        shadowView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1).isActive = true
        shadowView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1).isActive = true
        shadowView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        shadowView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        
        return shadowView
    }
    
}


extension UISearchBar{
    
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}
