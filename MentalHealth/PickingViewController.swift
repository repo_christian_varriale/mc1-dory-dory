//
//  PickingViewController.swift
//  MentalHealth
//
//  Created by Михаил on 22.11.2019.
//  Copyright © 2019 Power Rangers. All rights reserved.
//

import Foundation
import UIKit

class PickingViewController: UICollectionViewController  {
let emotion = Emotion()
    
override func viewDidLoad() {
        
        super.viewDidLoad()
        
        emotion.makeEmotions()
        print(self.collectionView.isUserInteractionEnabled)
    }
}

extension PickingViewController {
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                collectionView.deselectItem(at: indexPath, animated: true)
                if let index = emotion.selectedEmotionIndex,
                    let cell = collectionView.cellForItem(at: IndexPath(row: index, section: 0)) {
                    cell.backgroundColor = #colorLiteral(red: 0.6935172677, green: 0.4671925306, blue: 0.592478931, alpha: 0.8470588235)
                    print("cell non selected")
        
                }
                emotion.selectEmotion(at: indexPath)
        
                let cell = collectionView.cellForItem(at: indexPath)
                print(emotion.selectedEmotion)
                cell?.backgroundColor = #colorLiteral(red: 0.6935172677, green: 0.4671925306, blue: 0.592478931, alpha: 0.8470588235)
                print("\(indexPath.row): cell selected")
        
            performSegue(withIdentifier: "unwind", sender: cell)
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return emotion.numberOfEmotions()
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmotionCell", for: indexPath) as! EmotionCell
        
        cell.displayContent(image: emotion.emotions[indexPath.row].image, title: emotion.emotions[indexPath.row].descript)
        
        if indexPath.item == emotion.selectedEmotionIndex {
            cell.isSelected = true
        } else {
            cell.isSelected = false
        }
        
        return cell
    }
}

