//
//  EmotionData.swift
//  MentalHealth
//
//  Created by Михаил on 22.11.2019.
//  Copyright © 2019 Power Rangers. All rights reserved.
//

import Foundation
import UIKit

struct Emoticon2 {
    var descript: String
    var image : UIImage
    var rating : Int
    
    init(description: String, imag: UIImage) {
        descript = description
        image = imag
        rating = 0
    }
}

struct Activity2 {
    var descript: String
    var image : UIImage
    var rating : Int
    
    init(description: String, imag: UIImage) {
        descript = description
        image = imag
        rating = 0
    }
}

class Emotion: NSObject {
    
    var emotions : [Emoticon2] = []

    var descript = [
    "sad",
    "happy",
    "angry",
    "veryhappy",
    "amused",
    "confused",
    "suffering",
    "neutral",
    "cancel",
    "III",
    "nostalgic",
    "disappointed",
    ]

    var imageEmotion = [
        #imageLiteral(resourceName: "sad"),#imageLiteral(resourceName: "happy"), #imageLiteral(resourceName: "angry"), #imageLiteral(resourceName: "veryhappy"), #imageLiteral(resourceName: "amused"), #imageLiteral(resourceName: "confused"), #imageLiteral(resourceName: "suffering"), #imageLiteral(resourceName: "neutral"), #imageLiteral(resourceName: "cancel"), #imageLiteral(resourceName: "III"), #imageLiteral(resourceName: "nostalgic"), #imageLiteral(resourceName: "disappointed")]

    var selectedEmotionIndex: Int?

    func makeEmotions() {
        for i in 0...(descript.count - 1) {
            emotions.append(Emoticon2(description: descript[i], imag: imageEmotion[i]))
            print(descript[i], i)
        }
    }
    
    var selectedEmotion: String? {
        didSet {
            if let selectedEmotion = selectedEmotion,

                let index = descript.firstIndex(of: selectedEmotion)
            {
                print("here")
                selectedEmotionIndex = index
                
            }
        }
    }
    
    var selectedEmotionImage: UIImage? {
        didSet {
            if let selectedEmotionImage = selectedEmotionImage,
            
                let index = imageEmotion.firstIndex(of: selectedEmotionImage)
            {
                print("here")
                selectedEmotionIndex = index
            }
        }
    }

    func selectEmotion(at indexPath: IndexPath) {
        selectedEmotion = descript[indexPath.row]
        selectedEmotionImage = imageEmotion[indexPath.row]
    }
    
    
    
    func numberOfEmotions() -> Int {
        emotions.count
    }
}

class Active: NSObject {
    
    var activities : [Activity2] = []

    var descript = [
    "gym",
    "music",
    "sea",
    "travel",
    "film",
    "guitar",
    "book",
    "bike",
    "map",
    "basket",
    "cook",
    "drink",
    "shopping",
    "sleep",
    "tv",
    ]

    var imageActive = [
        #imageLiteral(resourceName: "gym"),
        #imageLiteral(resourceName: "music"),
        #imageLiteral(resourceName: "sea"),
        #imageLiteral(resourceName: "map"),
        #imageLiteral(resourceName: "film"),#imageLiteral(resourceName: "guitar"), #imageLiteral(resourceName: "book"), #imageLiteral(resourceName: "bike"), #imageLiteral(resourceName: "travel"), #imageLiteral(resourceName: "basket"), #imageLiteral(resourceName: "cook"), #imageLiteral(resourceName: "drink"), #imageLiteral(resourceName: "shopping"), #imageLiteral(resourceName: "sleep"), #imageLiteral(resourceName: "tv")]

    var selectedActiveIndex: Int?

    func makeActive() {
        for i in 0...(descript.count - 1) {
            activities.append(Activity2(description: descript[i], imag: imageActive[i]))
            print(descript[i], i)
        }
    }
    
    var selectedActive: String? {
        didSet {
            if let selectedActive = selectedActive,

                let index = descript.firstIndex(of: selectedActive)
            {
                print("here")
                selectedActiveIndex = index
                
            }
        }
    }
    
    var selectedActiveImage: UIImage? {
        didSet {
            if let selectedActiveImage = selectedActiveImage,
            
                let index = imageActive.firstIndex(of: selectedActiveImage) {
                selectedActiveIndex = index
            }
        }
    }

    func selectActive(at indexPath: IndexPath) {
        selectedActive = descript[indexPath.row]
        selectedActiveImage = imageActive[indexPath.row]
    }
    
    func numberOfActives() -> Int {
        activities.count
    }
}


