//
//  ShowMemoriesViewController.swift
//  MentalHealth
//
//  Created by Christian Varriale on 16/11/2019.
//  Copyright © 2019 Power Rangers. All rights reserved.
//

import Foundation
import UIKit

class ShowMemoriesViewController: UIViewController {
    
    public var scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = .white
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
        
    public var photoText : UILabel = {
        let photoText = UILabel()
        photoText.text = "Photo"
        photoText.font = UIFont(name: "HelveticaNeue-Bold", size: 22)
        photoText.textColor = .black
        photoText.numberOfLines = 1
        photoText.translatesAutoresizingMaskIntoConstraints = false
        return photoText
    }()
    
    private var stackView1: UIStackView = {
        let stackView1 = UIStackView()
        stackView1.axis = .horizontal
        stackView1.distribution = .fillProportionally
        stackView1.translatesAutoresizingMaskIntoConstraints = false
        return stackView1
    }()
    
    public var containerTextMemory : UIView = {
        let container = UIView()
        container.translatesAutoresizingMaskIntoConstraints = false
        container.layer.cornerRadius = 15
        container.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.4352941176, blue: 0.4352941176, alpha: 0.1)
        return container
        
    }()
    
    public var textMemorySelected : UITextView  = {
        let textMemory = UITextView ()
        textMemory.translatesAutoresizingMaskIntoConstraints = false
        //textMemory.isScrollEnabled = true
        //textMemory.showsVerticalScrollIndicator = true
        textMemory.isUserInteractionEnabled = false
        textMemory.font = UIFont(name: "HelveticaNeue", size: 12)
        textMemory.textColor = #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 1)
        textMemory.backgroundColor = .clear
        return textMemory
    }()
    
    public var selfieImage : UIImageView = {
        let selfieImage = UIImageView()
        selfieImage.translatesAutoresizingMaskIntoConstraints = false
        selfieImage.layer.masksToBounds = true
        selfieImage.layer.cornerRadius = 15
        selfieImage.contentMode = .scaleAspectFill
        
        return selfieImage
    }()
    
    public var imageActivity : UIImageView = {
        let imageActivity = UIImageView()
        imageActivity.translatesAutoresizingMaskIntoConstraints = false
        
        return imageActivity
    }()
    
    public var imageEmotion : UIImageView = {
        let imageEmotion = UIImageView()
        imageEmotion.translatesAutoresizingMaskIntoConstraints = false
        
        return imageEmotion
    }()
    
    public var emoticonText : UILabel = {
        let emoticonText = UILabel()
        emoticonText.translatesAutoresizingMaskIntoConstraints = false
        emoticonText.text = "Emoticon"
        emoticonText.font = UIFont(name: "HelveticaNeue-Bold", size: 22)
        emoticonText.textColor = .black
        emoticonText.numberOfLines = 1
        return emoticonText
    }()
    
    public var activityText : UILabel = {
        let activityText = UILabel()
        activityText.translatesAutoresizingMaskIntoConstraints = false
        activityText.text = "Activity"
        activityText.font = UIFont(name: "HelveticaNeue-Bold", size: 22)
        activityText.textColor = .black
        activityText.numberOfLines = 1
        return activityText
    }()
    
    public var stackActEmo : UIStackView = {
         let stackActEmo = UIStackView()
         stackActEmo.axis = .horizontal
         stackActEmo.translatesAutoresizingMaskIntoConstraints = false
         stackActEmo.distribution = .fillProportionally
         return stackActEmo
         
     }()
    
    
    
    var memorySelected : Memory?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textMemorySelected.text = memorySelected?.someText
        selfieImage.image = memorySelected?.imageContent
        imageActivity.image = UIImage(named: memorySelected!.activity)
        imageEmotion.image = UIImage(named: memorySelected!.emoticon)
        
        
        setUpConstraint()
        
    }
    
    
    func setUpConstraint(){
        
        self.view.addSubview(scrollView)
        scrollView.addSubview(photoText)
        scrollView.addSubview(selfieImage)
        scrollView.addSubview(stackActEmo)
        scrollView.addSubview(imageEmotion)
        scrollView.addSubview(imageActivity)
        scrollView.addSubview(containerTextMemory)
        containerTextMemory.addSubview(textMemorySelected)
        stackActEmo.addArrangedSubview(emoticonText)
        stackActEmo.addArrangedSubview(activityText)
        
        
        NSLayoutConstraint.activate([
        
            scrollView.topAnchor.constraint(equalTo: self.view.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            
            photoText.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 30),
            photoText.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 16),
            photoText.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -16),
            photoText.heightAnchor.constraint(equalToConstant: 20),
            
            selfieImage.topAnchor.constraint(equalTo: photoText.bottomAnchor, constant: 40),
            selfieImage.widthAnchor.constraint(equalToConstant: 290),
            selfieImage.heightAnchor.constraint(equalToConstant: 290),
            selfieImage.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
            
            stackActEmo.topAnchor.constraint(equalTo: selfieImage.bottomAnchor, constant: 30),
            stackActEmo.heightAnchor.constraint(equalToConstant: 25),
            stackActEmo.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 95),
            stackActEmo.widthAnchor.constraint(equalToConstant: 260),
              
            imageEmotion.topAnchor.constraint(equalTo: stackActEmo.bottomAnchor, constant: 15),
            imageEmotion.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 110),
            imageEmotion.heightAnchor.constraint(equalToConstant: 70),
            imageEmotion.widthAnchor.constraint(equalToConstant: 70),
            
            imageActivity.topAnchor.constraint(equalTo: stackActEmo.bottomAnchor, constant: 15),
            imageActivity.leadingAnchor.constraint(equalTo: imageEmotion.leadingAnchor, constant: 135),
            imageActivity.heightAnchor.constraint(equalToConstant: 70),
            imageActivity.widthAnchor.constraint(equalToConstant: 70),
            
            containerTextMemory.topAnchor.constraint(equalTo: imageActivity.bottomAnchor, constant : 40),
            containerTextMemory.heightAnchor.constraint(equalToConstant: 200),
            containerTextMemory.widthAnchor.constraint(equalToConstant: 350),
            containerTextMemory.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 33),
            
            textMemorySelected.topAnchor.constraint(equalTo: containerTextMemory.topAnchor, constant: 10),
            textMemorySelected.leadingAnchor.constraint(equalTo: containerTextMemory.leadingAnchor, constant: 10),
            textMemorySelected.trailingAnchor.constraint(equalTo: containerTextMemory.trailingAnchor, constant: -10),
            textMemorySelected.bottomAnchor.constraint(equalTo: containerTextMemory.bottomAnchor, constant: -10)
        
        
        
        
        ])
        let tr = CGAffineTransform.identity.rotated(by: .pi/2)
        selfieImage.transform = tr
        selfieImage.addShadowViewRotate()
        

    }
    
}


