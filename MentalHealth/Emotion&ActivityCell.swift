//
//  Emotion&ActivityCell.swift
//  MentalHealth
//
//  Created by Михаил on 17.11.2019.
//  Copyright © 2019 Power Rangers. All rights reserved.
//

import Foundation
import UIKit

class EACell: UICollectionViewCell {
    
    @IBOutlet var picture: UIImageView!
    @IBOutlet var descript: UILabel!
    
    func displayContent(image: UIImage, title: String) {
        
        picture.image = image
        descript.text = title
    }
    
//    var selectedEmotion: Int
    
    override var isSelected: Bool {
      didSet {
        picture.layer.borderWidth = isSelected ? 10 : 0
        
        
      }
    }
//    override func prepare(for segue: UI) {
//        <#code#>
//    }
//    selectedEmotion = segue.destination as! AddMemoryViewController
}
